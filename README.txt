-- SUMMARY --

This module provides interface for admin to add email address if Admin want to 
get mail notification when new comment has been posted using facebook comment 
module.
Multiple email addresses can be added separated by comma. 

-- REQUIREMENTS --

facebook comment module.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Add email address in Administer >> Site configuration >>
  Facebook comments notification settings.


-- CONTACT --

Current maintainers:
* Pragna Bhalsod (pragna) - https://www.drupal.org/u/pragna
